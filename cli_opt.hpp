//
//  cli_opt.h
//  biGraph
//
//  Created by 曹巍 on 2017/02/21.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef cli_opt_h
#define cli_opt_h

#include <sys/stat.h>
#include "common.hpp"

namespace po = boost::program_options;

namespace VersionInfo{
  std::string version("biGraph Version 1.0 developped by Wei Cao.");
}

namespace CLIARG {
  // General options
  bool verbose = false;
  bool directed = false;
  //int numOfvertex = 0;
  //int graphType = 1;
  std::string o_filename;
  //std::string o_graph_name;
  std::string i_filename;
  std::string inputfile_size; // store the size of input file.
  
  // all the options
  po::options_description general("General options");
  po::options_description opt("Graph options");
  
  // Function: Initialize opts objects
  void init(){
    general.add_options()
    ("version,V", "Show the version number")
    //("num,n", po::value<int>(),"Specify the total number of vertices. Default is 0.")
    //("type,t",po::value<int>(),"Graph types: 0->directed(not implemented yet) or 1->undirected. Default is 1.")
    //("infile,f",po::value<std::string>(&i_filename)->required(),"The input data file. Only the dot format of graphviz is supported now.")
    ("infile,i",po::value<std::string>(&i_filename),"The input data file. Only the dot format of graphviz is supported now.")
    ("outfile,o", po::value<std::string>(&o_filename),"The output file of clusters in text format.Default name is 'out.clust'.")
    //("graph,g", po::value<std::string>(),"The output file in the dot format of Graphviz.")
    ("help,h", "print help info.")
    ;
    
    opt.add_options()
    ("verbose,v","The extra verbose.")
    //("rmin",po::value<double>(),"The minimum rmsd for the cutoff between two pdb structures. For rmsd values of two structures, i.e., rmin<= r <= rmax, the edge will be created for the graph. rmin=0.0.")
    //("rmax",po::value<double>(),"The maximium rmsd for the cutoff between two pdb structures. rmax = +inf.")
    //("sim1,a",po::value<double>(),"The similarity cutoff for the structure A in the pairwised structure alignment. Together with rmsd cutoff. Default is zero.")
    //("sim2,b",po::value<double>(),"The similarity cutoff for the structure B in the pairwised structure alignment. Used with rmsd cutoff together. Default is zero.")
    //("optlen,l",po::value<int>(),"The optimal length cutoff for structure comparison.Default is 35 aa.")
    ;
    
    general.add(opt);
  }
  
  // Function: Check the existance of the file
  //
  bool FileExists(std::string fname) {
    struct stat FileInfo;
    int val;
    val = stat(fname.c_str(), &FileInfo);
    if(FileInfo.st_size >1000000){
      inputfile_size = byteConverter_s(FileInfo.st_size);
      std::cout<<fname<<" is so large ("<<inputfile_size<<")"<<". Take a while to read..."<<std::endl;
    }
    return (val == 0) ? true : false;
  }

  // Function: Parse cmdline arguments
  //
  void ParseCmdLine(int argc, const char * argv[]) {
    //
    //parse command lines
    po::variables_map vm;
    try{
      // initialize options
      init();
      po::store(parse_command_line(argc, argv, general),vm);
      po::notify(vm);
    }catch(po::ambiguous_option& e){
      //std::cout<<"Program is terminated for the following reason(s):\n\n";
      std::cout<<e.get_option_name()<<" is an ambiguous option name.\n";
      std::cout << general << std::endl;
      exit(0);
      //BOOST_CHECK_EQUAL(std::string(e.what()), "Unknown option");
    }catch(po::unknown_option& e){
      std::cout<<e.get_option_name()<<" is an unknown option name.\n";
      std::cout << general << std::endl;
      exit(0);
      //BOOST_CHECK_EQUAL(e.get_option_name(), "option name");
      //BOOST_CHECK_EQUAL(std::string(e.what()), "Unknown option");
    }
    
    if( vm.count("help") ){
      // print help info
      std::cout << general << std::endl;
      exit(0);
    }
    
    if( vm.count("version") ){
      //print version info
      std::cout << VersionInfo::version << std::endl;
      exit(0);
    }
    
    if ( vm.count("verbose") ){
      verbose = true;
    }
  /*
    if ( vm.count("num") ) {
      numOfvertex = vm["num"].as<int>();
    }
    
    if ( vm.count("type") ) {
      graphType = vm["type"].as<int>();
    }

    if ( vm.count("graph") ) {
      o_graph_name = vm["graph"].as<std::string>();
    }

   */
    
    if ( vm.count("outfile") ) {
      o_filename = vm["outfile"].as<std::string>();
    }
    
    // Required arguments
    if ( vm.count("infile") ) {
      i_filename = vm["infile"].as<std::string>();
      if( !FileExists(i_filename) ) {
        std::cout << "Could not find the file: "<< i_filename <<std::endl;
        std::cout << "Program terminated." << std::endl;
        exit(0);
      }
    }else{
      std::cout << general << std::endl;
      exit(0);
    }
  }
}
#endif /* cli_opt_h */

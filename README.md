#### biGraph
Implement algorithms of graph theory being heavily depended on Boost Graph Library.

##### Prerequisite
1. CMake
2. C++ compiler with C++14 support 
	tested with intel compiler icpc (ICC) 18.0.0 20170320
##### Download
	git clone https://davecao_00@bitbucket.org/davecao_00/bigraph.git

##### Installation
	CXX=icpc CC=icc cmake -DCMAKE_INSTALL_PREFIX=/path/to/install ../bigraph
	make
	make install

##### Usage

General options:  
  -V [ --version ]      Show the version number  
  -i [ --infile ] arg      The input data file. Only the dot format of graphviz  
                               is supported now.  
  -o [ --outfile ] arg   The output file of clusters in text format.Default  
                                name is 'out.clust'.  
  -h [ --help ]            print help info.  

Graph options:  
  -v [ --verbose ]       The extra verbose.  

##### Trouble shooting
1. Boost dynamic libraries or biGraph dynamic library

dyld: Library not loaded: @rpath/libbiGraph.dylib
Reason: image not found

dyld: Library not loaded: @rpath/libboost_program_options.dylib
  Referenced from: /path/to/bin/biGraph
  Reason: image not found
[1]    22595 abort      ../test_bin/bin/biGraph

Solution: 

export DYLD_FALLBACK_LIBRARY_PATH= /path/to/boost/lib:/path/to/libGraph/lib:$DYLD_FALLBACK_LIBRARY_PATH
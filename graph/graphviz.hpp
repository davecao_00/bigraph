//
//  graphviz.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/07.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef graphviz_h
#define graphviz_h

#include <string>
#include <map>
#include <unordered_set>
#include <iostream>
#include <fstream>
#include <stdio.h> // for FILE

#include <boost/config.hpp>
#include <boost/version.hpp>
#if (BOOST_VERSION >= 104000)
#   include <boost/property_map/property_map.hpp>
#   include <boost/property_map/dynamic_property_map.hpp>
#else
#   include <boost/property_map.hpp>
#   include <boost/dynamic_property_map.hpp>
#endif
#include <boost/tuple/tuple.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/subgraph.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/overloading.hpp>

#ifdef BOOST_HAS_DECLSPEC
#  if defined(BOOST_ALL_DYN_LINK) || defined(BOOST_GRAPH_DYN_LINK)
#    ifdef BOOST_GRAPH_SOURCE
#      define BOOST_GRAPH_DECL __declspec(dllexport)
#    else
#      define BOOST_GRAPH_DECL __declspec(dllimport)
#    endif  // BOOST_GRAPH_SOURCE
#  endif  // DYN_LINK
#endif  // BOOST_HAS_DECLSPEC

#ifndef BOOST_GRAPH_DECL
#  define BOOST_GRAPH_DECL
#endif

#ifdef BOOST_GRAPH_USE_SPIRIT_PARSER
#  ifndef BOOST_GRAPH_READ_GRAPHVIZ_ITERATORS
#    define BOOST_GRAPH_READ_GRAPHVIZ_ITERATORS
#  endif
#  include <boost/graph/detail/read_graphviz_spirit.hpp>
#else // New default parser
#  include "detail/read_graphviz_new.hpp"
#endif // BOOST_GRAPH_USE_SPIRIT_PARSER

namespace boost{

namespace detail {
namespace graph {
  BOOST_GRAPH_DECL
  bool read_graphviz(std::istream& in, mutate_graph& graph);
}// namespace graph
}// namespace detail
  
  // Parse the passed stream as a GraphViz dot file.
  template <typename MutableGraph>
  bool read_graphviz(std::istream& in, MutableGraph& graph,
                     dynamic_properties& dp,
                     std::map<std::string, typename graph_traits<MutableGraph>::vertex_descriptor>& vp,
                     std::string const& node_id = "node_id",
                     bool ignore_directedness = false,
                     const std::unordered_set<std::string>& ignore_vp = std::unordered_set<std::string>(),
                     const std::unordered_set<std::string>& ignore_ep = std::unordered_set<std::string>(),
                     const std::unordered_set<std::string>& ignore_gp = std::unordered_set<std::string>())
  {
    std::string data;
    in >> std::noskipws;
    std::copy(std::istream_iterator<char>(in),
              std::istream_iterator<char>(),
              std::back_inserter(data));
    // read_graphviz defined in detail/read_graphviz_new.hpp
    return read_graphviz(data, graph, dp, vp, node_id, ignore_directedness, ignore_vp,
                         ignore_ep, ignore_gp);
  }
} // namespace boost


#endif /* graphviz_h */

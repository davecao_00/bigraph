//
//  graph_selectors.cpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/15.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#include "graph_filtering.hpp"
#include "graph.hpp"
#include "graph_selectors.hpp"
#include <boost/variant/get.hpp>


struct bilab::get_degree_selector
{
  typedef boost::mpl::map
  < boost::mpl::pair<in_degreeS,
  boost::mpl::int_<IGraph::IN_DEGREE> >,
  boost::mpl::pair<out_degreeS,
  boost::mpl::int_<IGraph::OUT_DEGREE> >,
  boost::mpl::pair<total_degreeS,
  boost::mpl::int_<IGraph::TOTAL_DEGREE> >
  > degree_selector_index;
  
  template <class Selector>
  void operator()(Selector, int deg_index, boost::any& deg) const
  {
    if (boost::mpl::at<degree_selector_index, Selector>::type::value == deg_index){
      deg = Selector();
    }
  }
};


boost::any bilab::degree_selector(IGraph::deg_t deg){
 boost::any sel;
 IGraph::degree_t* d = boost::get<IGraph::degree_t>(&deg);
 
 if (d != 0)
 {
   boost::mpl::for_each<selectors>
   (std::bind(bilab::get_degree_selector(), std::placeholders::_1, *d,
               std::ref(sel)));
 }
 else
 {
   boost::any* d = boost::get<boost::any>(&deg);
   bool found = false;
   boost::mpl::for_each<vertex_properties>
    (std::bind(get_scalar_selector(), std::placeholders::_1, *d,
               std::ref(sel), std::ref(found)));
   if (!found)
     throw bilab::ValueException("invalid degree selector");
  }
  return sel;
 }

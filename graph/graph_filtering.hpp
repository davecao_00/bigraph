//
//  graph_filtering.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/02/21.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef graph_filtering_hpp
#define graph_filtering_hpp


#include <type_traits>

#include <boost/version.hpp>
#include <boost/range/irange.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/filtered_graph.hpp>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/erase.hpp>
#include <boost/mpl/clear.hpp>
#include <boost/mpl/map.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/or.hpp>
#include <boost/mpl/if.hpp>
#include <boost/mpl/logical.hpp>
#include <boost/mpl/inserter.hpp>
#include <boost/mpl/insert_range.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/mpl/plus.hpp>
#include <boost/mpl/divides.hpp>
#include <boost/mpl/arithmetic.hpp>
#include <boost/mpl/greater_equal.hpp>
#include <boost/mpl/comparison.hpp>
#include <boost/mpl/transform_view.hpp>
#include <boost/mpl/quote.hpp>
#include <boost/mpl/range_c.hpp>
#include <boost/mpl/print.hpp>

#include "graph_adaptor.hpp"
#include "graph_filtered.hpp"
#include "graph_reverse.hpp"
#include "graph_util.hpp"
#include "mpl_nested_loop.hpp"
#include "demangle.hpp"

//#include "graph_properties.hpp"
#include "graph_exceptions.hpp"
#include "graph.hpp"

namespace bilab
{
  //Forward declaration
  class IGraph;
  // defined in graph_selectors.hpp
  template <class PropertyMap>
  struct scalarS;
  
  // Whenever no implementation is called, the following exception is thrown
  class ActionNotFound: public GraphException
  {
  public:
    ActionNotFound(const std::type_info& action,
                   const std::vector<const std::type_info*>& args);
    virtual ~ActionNotFound() throw () {}
  private:
    const std::type_info& _action;
    std::vector<const std::type_info*> _args;
  };
  
  namespace detail
  {
    // Implementation
    // --------------
    //
    // The class MaskFilter below is the main filter predicate for the filtered
    // graph view, based on descriptor property maps.  It filters out edges or
    // vertices which are masked according to a property map with bool (actually
    // uint8_t) value type.
    
    template<class DescriptorProperty>
    class MaskFilter
    {
    public:
      typedef typename boost::property_traits<DescriptorProperty>::value_type value_t;
      MaskFilter()
      {}
      
      MaskFilter(DescriptorProperty& filtered_property, bool& invert)
      : _filtered_property(&filtered_property), _invert(&invert)
      {}
      
      template <class Descriptor>
      inline bool operator() (Descriptor&& d) const
      {
        // ignore if masked
        return get(*_filtered_property, std::forward<Descriptor>(d)) ^ *_invert;
        
        // This is a critical section. It will be called for every vertex or
        // edge in the graph, every time they're iterated through.
      }
      
      DescriptorProperty& get_filter() { return *_filtered_property; }
      bool is_inverted()
      { return *_invert; }
      
    private:
      DescriptorProperty* _filtered_property;
      bool* _invert;
    };

    
    // Metaprogramming
    // ---------------
    //
    // We need to generate a type sequence with all the filtered graph views, which
    // will be called all_graph_views.
    // metafunction class to get the correct filter predicate
    template <class Property>
    struct get_predicate
    {
      typedef MaskFilter<Property> type;
    };
    
    template <>
    struct get_predicate<boost::keep_all>
    {
      typedef boost::keep_all type;
    };

    // metafunction to get the filtered graph type
    struct graph_filter
    {
      template <class Graph, class EdgeProperty, class VertexProperty>
      struct apply
      {
        
        typedef typename get_predicate<EdgeProperty>::type edge_predicate;
        typedef typename get_predicate<VertexProperty>::type vertex_predicate;
        
        typedef boost::filt_graph<
          Graph, edge_predicate, vertex_predicate
        > filtered_graph_t;
        
        // If both predicates are keep_all, then return the original graph
        // type. Otherwise return the filtered_graph type.
        typedef typename boost::mpl::if_<
          typename boost::mpl::and_<
            std::is_same<edge_predicate, boost::keep_all>,
            std::is_same<vertex_predicate,boost::keep_all>
          >::type,
          Graph,
          filtered_graph_t>::type type;
      };
    };
    
    // metafunction to get the undirected graph type
    struct graph_undirect
    {
      template <class Graph>
      struct apply
      {
        typedef boost::undirected_adaptor<Graph> type;
      };
    };
    
    // metafunction to get the reversed graph type
    struct graph_reverse
    {
      template <class Graph>
      struct apply
      {
        typedef boost::reversed_graph<Graph> type;
      };
    };

    // metafunction class to get the correct property map type
    template <class Scalar, class IndexMap>
    struct get_property_map_type
    {
      typedef typename property_map_type::apply<Scalar, IndexMap>
        ::type::unchecked_t type;
    };
    
    template <class IndexMap>
    struct get_property_map_type<boost::keep_all, IndexMap>
    {
      typedef boost::keep_all type;
    };
    
    // this metafunction returns a filtered graph type
    struct get_graph_filtered
    {
      template<class Graph>
      struct apply
      {
        // if the 'scalar' is the index map itself, use simply that, otherwise
        // get the specific property map
        typedef typename get_property_map_type<
            uint8_t,
        typename bilab::IGraph::edge_index_map_t>::type edge_property_map;
        
        typedef typename get_property_map_type<
          uint8_t,
        typename bilab::IGraph::vertex_index_map_t>::type vertex_property_map;

        typedef typename graph_filter::apply<
            Graph,
            edge_property_map,
            vertex_property_map>::type type;
      };
    };

    // this metafunction returns all the possible graph views
    struct get_all_graph_views
    {
      template <class FiltType,
        class AlwaysDirected = boost::mpl::bool_<false>,
        class NeverDirected = boost::mpl::bool_<false>,
        class AlwaysReversed = boost::mpl::bool_<false>,
        class NeverReversed = boost::mpl::bool_<false>,
        class NeverFiltered = boost::mpl::bool_<false> >
      struct apply
      {
        struct base_graphs
          : boost::mpl::vector1<IGraph::multigraph_t>
        {};
        
        // reversed graphs
        struct reversed_graphs:
          boost::mpl::if_<
            AlwaysReversed,
            typename boost::mpl::transform<base_graphs, graph_reverse>::type,
            typename boost::mpl::if_<
              NeverReversed,
              base_graphs,
              typename boost::mpl::transform<
                base_graphs,
                graph_reverse,
                boost::mpl::back_inserter<base_graphs>
              >::type
          >::type
        >::type
        {};
        
        // undirected graphs
        struct undirected_graphs:
          boost::mpl::if_<
            AlwaysDirected,
            reversed_graphs,
            typename boost::mpl::if_<
              NeverDirected,
              typename boost::mpl::transform<
                base_graphs,
                graph_undirect,
                boost::mpl::back_inserter<boost::mpl::vector0<>>
              >::type,
              typename boost::mpl::transform<
                base_graphs,
                graph_undirect,
                boost::mpl::back_inserter<reversed_graphs>
              >::type
          >::type
        >::type
        {};
        
        // filtered graphs
        struct filtered_graphs:
          boost::mpl::if_<NeverFiltered, undirected_graphs,
            typename boost::mpl::transform<
              undirected_graphs,
              get_graph_filtered,
              boost::mpl::back_inserter<undirected_graphs>>::type
          >::type {};
        
        typedef filtered_graphs type;
      };
    };
    
    typedef uint8_t filt_scalar_type;
    
    // finally, this type should hold all the possible graph views
    struct all_graph_views:
      get_all_graph_views::apply<filt_scalar_type>::type {};
    
    // restricted graph views
    struct always_directed:
      get_all_graph_views::apply<
        filt_scalar_type,
        boost::mpl::bool_<true> >::type {};
    
    struct never_directed:
      get_all_graph_views::apply<
        filt_scalar_type,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<true> >::type {};
    
    struct always_reversed:
      get_all_graph_views::apply<
        filt_scalar_type,
        boost::mpl::bool_<true>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<true> >::type {};
    
    struct never_reversed:
      get_all_graph_views::apply<
        filt_scalar_type,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<true> >::type {};
    
    struct always_directed_never_reversed:
      get_all_graph_views::apply<
        filt_scalar_type,
        boost::mpl::bool_<true>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<true> >::type {};
    
    struct never_filtered:
      get_all_graph_views::apply<
        filt_scalar_type,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<true> >::type {};
    
    struct never_filtered_never_reversed:
      get_all_graph_views::apply<
        filt_scalar_type,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<false>,
        boost::mpl::bool_<true>,
        boost::mpl::bool_<true> >::type {};
  
    // sanity check
    typedef boost::mpl::size<all_graph_views>::type n_views;
    BOOST_MPL_ASSERT_RELATION(n_views::value, == , boost::mpl::int_<6>::value);
    
    // wrap action to be called, to deal with property maps,
    // i.e., return version
    // with no bounds checking.
    template <class Action, class Wrap>
    struct action_wrap
    {
      action_wrap(Action a) : _a(a) {}
      
      template <class Type, class IndexMap>
      auto& uncheck(boost::checked_vector_property_map<Type,IndexMap>& a,
                    boost::mpl::true_) const
      {
        return a;
      }
      
      template <class Type, class IndexMap>
      auto uncheck(boost::checked_vector_property_map<Type, IndexMap>& a,
                   boost::mpl::false_) const
      {
        return a.get_unchecked();
      }
      
      template <class Type>
      auto uncheck(scalarS<Type>& a, boost::mpl::false_) const
      {
        auto pmap = uncheck(a._pmap, boost::mpl::false_());
        return scalarS<decltype(pmap)>(pmap);
      }
      
      //no op
      template <class Type, class DoWrap>
      Type& uncheck(Type&& a, DoWrap) const { return a; }
      
      template <class Type>
      auto& deference(Type* a) const
      {
        typedef typename std::remove_const<Type>::type type_t;
        typedef typename boost::mpl::find<detail::all_graph_views, type_t>::type iter_t;
        typedef typename boost::mpl::end<detail::all_graph_views>::type end_t;
        return deference_dispatch(a, typename std::is_same<iter_t, end_t>::type());
      }
      
      template <class Type>
      auto& deference_dispatch(Type*& a, std::true_type) const
      {
        return a;
      }
      
      template <class Type>
      Type& deference_dispatch(Type* a, std::false_type) const
      {
        return *a;
      }
      
      template <class Type>
      Type& deference(Type&& a) const
      {
        return a;
      }
      
      template <class... Ts>
      void operator()(Ts&&... as) const
      {
        _a(deference(uncheck(std::forward<Ts>(as), Wrap()))...);
      }
      
      Action _a;
    };
    
    // this takes a functor and type ranges and iterates through the type
    // combinations when called with boost::any parameters, and calls the correct
    // function
    template <class Action, class Wrap, class... TRS>
    struct action_dispatch
    {
      action_dispatch(Action a) : _a(a) {}
      
      template <class... Args>
      void operator()(Args&&... args) const
      {
        bool found = boost::mpl::nested_for_each<TRS...>(
                          _a,
                          std::forward<Args>(args)...);
        if (!found)
        {
          std::vector<const std::type_info*> args_t = {(&(args).type())...};
          throw ActionNotFound(typeid(Action), args_t);
        }
      }
      
      action_wrap<Action, Wrap> _a;
    };
  } // details namespace
  
  // dispatch "Action" across all type combinations
  template <
    class GraphViews = detail::all_graph_views,
    class Wrap = boost::mpl::false_>
  struct run_action
  {
    template <class Action, class... TRS>
    auto operator()(bilab::IGraph& gi, Action a, TRS...)
    {
      auto dispatch = detail::action_dispatch<
                      Action,
                      Wrap,
                      GraphViews,TRS...>(a);
      auto wrap = [dispatch, &gi](auto&&... args) {
          dispatch(gi.get_graph_view(), args...);
      };
      return wrap;
    }
  };
  
  template <class Wrap = boost::mpl::false_>
  struct gt_dispatch
  {
    template <class Action, class... TRS>
    auto operator()(Action a, TRS...)
    {
      return detail::action_dispatch<Action,Wrap,TRS...>(a);
    }
  };
  
  typedef detail::all_graph_views all_graph_views;
  typedef detail::always_directed always_directed;
  typedef detail::never_directed never_directed;
  typedef detail::always_reversed always_reversed;
  typedef detail::never_reversed never_reversed;
  typedef detail::always_directed_never_reversed always_directed_never_reversed;
  typedef detail::never_filtered never_filtered;
  typedef detail::never_filtered_never_reversed never_filtered_never_reversed;
  
  // returns true if graph filtering was enabled at compile time
  bool graph_filtering_enabled();
  
  template <class Graph, class GraphInit>
  std::shared_ptr<Graph>
  get_graph_ptr(bilab::IGraph& gi, GraphInit&, std::true_type)
  {
    return gi.get_graph_ptr();
  }
  
  template <class Graph, class GraphInit>
  std::shared_ptr<Graph>
  get_graph_ptr(bilab::IGraph&, GraphInit& g, std::false_type)
  {
    return std::make_shared<Graph>(g);
  }
  
  // this function retrieves a graph view stored in graph_views, or stores one if
  // non-existent
  template <class Graph>
  typename std::shared_ptr<Graph>
  retrieve_graph_view(bilab::IGraph& gi, Graph& init)
  {
    typedef typename std::remove_const<Graph>::type g_t;
    size_t index = boost::mpl::find<detail::all_graph_views,g_t>::type::pos::value;
    auto& graph_views = gi.get_graph_views();
    if (index >= graph_views.size())
      graph_views.resize(index + 1);
    boost::any& gview = graph_views[index];
    std::shared_ptr<g_t>* gptr = boost::any_cast<std::shared_ptr<g_t>>(&gview);
    if (gptr == 0)
    {
      std::shared_ptr<g_t> new_g =
          get_graph_ptr<g_t>(
                             gi, init,
                             std::is_same<g_t, bilab::IGraph::multigraph_t>());
      gview = new_g;
      return new_g;
    }
    return *gptr;
  }
  
  // this will check whether a graph is filtered and return the proper view
  // encapsulated
  template <
  class Graph,
  class EdgeFilter,
  class VertexFilter
  >
  boost::any
  check_filtered(const Graph& g,
                 const EdgeFilter& edge_filter,
                 const bool& e_invert,
                 bool e_active,
                 size_t max_eindex,
                 const VertexFilter& vertex_filter,
                 const bool& v_invert,
                 bool v_active,
                 bilab::IGraph& gi,
                 bool reverse,
                 bool directed)
  {
    auto check_filt = [&](auto&& u) -> boost::any
    {
      typedef typename std::remove_const<
      typename std::remove_reference<decltype(u)>::type
      >::type g_t;
      
      if (e_active || v_active) {
        bilab::detail::MaskFilter<EdgeFilter>
        e_filter(const_cast<EdgeFilter&>(edge_filter),
                 const_cast<bool&>(e_invert));
        
        bilab::detail::MaskFilter<VertexFilter>
        v_filter(const_cast<VertexFilter&>(vertex_filter),
                 const_cast<bool&>(v_invert));
        
        if (max_eindex > 0)
          edge_filter.reserve(max_eindex);
        if (num_vertices(g) > 0)
          vertex_filter.reserve(num_vertices(g));
        
        typedef boost::filt_graph<g_t,
        bilab::detail::MaskFilter<EdgeFilter>,
        bilab::detail::MaskFilter<VertexFilter>> fg_t;
        
        fg_t init(u, e_filter, v_filter);
        fg_t& fg = *retrieve_graph_view(gi, init);
        return std::ref(fg);
      } else {
        return std::ref(const_cast<g_t&>(u));
      }
    };
    
    auto check_reverse = [&](auto&& u) -> boost::any
    {
      typedef typename std::remove_const<
      typename std::remove_reference<decltype(u)>::type
      >::type g_t;
      
      if (reverse) {
        typedef typename
        std::conditional<
        std::is_const<g_t>::value,
        const boost::reversed_graph<typename std::remove_const<g_t>::type>,
        boost::reversed_graph<g_t>
        >::type reversed_graph_t;
        
        reversed_graph_t rg(u);
        
        return check_filt(*retrieve_graph_view(gi, rg));
      }
      return check_filt(u);
    };
    
    auto check_directed = [&](auto&& u) -> boost::any
    {
      typedef typename std::remove_const<
      typename std::remove_reference<decltype(u)>::type
      >::type g_t;
      
      if (directed){
        return check_reverse(u);
      }
      typedef boost::undirected_adaptor<g_t> ug_t;
      ug_t ug(u);
      return check_filt(*retrieve_graph_view(gi, ug));
    };
    
    return check_directed(g);
  }
  
} // namespace bilab


// Overload add_vertex() and add_edge() to filtered graphs, so that the new
// descriptors are always valid

namespace boost {
  template <class Graph, class EdgeProperty, class VertexProperty>
  auto add_vertex(
        boost::filt_graph<Graph,
                  bilab::detail::MaskFilter<EdgeProperty>,
                  bilab::detail::MaskFilter<VertexProperty>>& g)
  {
    auto v = add_vertex(const_cast<Graph&>(g._g));
    auto& filt = g._vertex_pred.get_filter();
    auto cfilt = filt.get_checked();
    cfilt[v] = !g._vertex_pred.is_inverted();
    return v;
  }
  
  template <class Graph, class EdgeProperty, class VertexProperty, class Vertex>
  auto
  add_edge(Vertex s, Vertex t, filt_graph<Graph,
           bilab::detail::MaskFilter<EdgeProperty>,
           bilab::detail::MaskFilter<VertexProperty>>& g)
  {
    auto e = add_edge(s, t, const_cast<Graph&>(g._g));
    auto& filt = g._edge_pred.get_filter();
    auto cfilt = filt.get_checked();
    cfilt[e.first] = !g._edge_pred.is_inverted();
    return e;
  }
  
  
  // Used to skip filtered vertices
  
  template <class Graph, class EP, class VP, class Vertex>
  bool is_valid_vertex(Vertex v, const boost::filt_graph<Graph,EP,VP>&)
  {
    return v != graph_traits<boost::filt_graph<Graph,EP,VP>>::null_vertex();
  }
  
  template <class Graph, class Vertex>
  bool is_valid_vertex(Vertex v, const boost::reversed_graph<Graph>& g)
  {
    return is_valid_vertex(v, g._g);
  }
  
  template <class Graph, class Vertex>
  bool is_valid_vertex(Vertex v, const boost::undirected_adaptor<Graph>& g)
  {
    return is_valid_vertex(v, g.original_graph());
  }
  
  template <class Graph, class Vertex>
  bool is_valid_vertex(Vertex, const Graph&)
  {
    return true;
  }

}

#endif /* graph_filtering_hpp */

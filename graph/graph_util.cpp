//
//  graph_util.cpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/09.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#include <iostream>
#include <clocale>

#include "graph_util.hpp"

//
// Data type string representation
// ===============================
//
// String representation of individual data types. We have to take care
// specifically that no information is lost with floating point I/O.
//

namespace boost
{
  //
  // "chars" should be printed as numbers, since they can be non-printable
  //
  template <>
  std::string lexical_cast<std::string,uint8_t>(const uint8_t& val){
    return lexical_cast<std::string>(int(val));
  }
  
  template <>
  uint8_t lexical_cast<uint8_t,std::string>(const std::string& val){
    return uint8_t(lexical_cast<int>(val));
  }
  //
  // float and double
  //
  // float, double and long double should be printed in hexadecimal format to
  // preserve internal representation. (we also need to make sure the
  // representation is locale-independent).
  
  int print_float_dispatch(char*& str, float val) {
    return asprintf(&str, "%a", val);
  }
  
  int print_float_dispatch(char*& str, double val) {
    return asprintf(&str, "%la", val);
  }
  
  int print_float_dispatch(char*& str, long double val) {
    return asprintf(&str, "%La", val);
  }
  
  template <class Val>
  int print_float(char*& str, Val val) {
    char* locale = setlocale(LC_NUMERIC, NULL);
    setlocale(LC_NUMERIC, "C");
    int retval = print_float_dispatch(str, val);
    setlocale(LC_NUMERIC, locale);
    return retval;
  }
  
  int scan_float_dispatch(const char* str, float& val) {
    return sscanf(str, "%a", &val);
  }
  
  int scan_float_dispatch(const char* str, double& val) {
    return sscanf(str, "%la", &val);
  }
  
  int scan_float_dispatch(const char* str, long double& val) {
    return sscanf(str, "%La", &val);
  }
  
  template <class Val>
  int scan_float(const char* str, Val& val) {
    char* locale = setlocale(LC_NUMERIC, NULL);
    setlocale(LC_NUMERIC, "C");
    int retval = scan_float_dispatch(str, val);
    setlocale(LC_NUMERIC, locale);
    return retval;
  }
  
  template <>
  std::string lexical_cast<std::string,float>(const float& val) {
    char* str = 0;
    int retval = print_float(str, val);
    if (retval == -1)
      throw bad_lexical_cast();
    std::string ret = str;
    free(str);
    return ret;
  }
  
  template <>
  float lexical_cast<float,std::string>(const std::string& val) {
    float ret;
    int nc = scan_float(val.c_str(), ret);
    if (nc != 1)
      throw bad_lexical_cast();
    return ret;
  }
  
  template <>
  std::string lexical_cast<std::string,double>(const double& val) {
    char* str = 0;
    int retval = print_float(str, val);
    if (retval == -1)
      throw bad_lexical_cast();
    std::string ret = str;
    free(str);
    return ret;
  }
  
  template <>
  double lexical_cast<double,std::string>(const std::string& val) {
    double ret;
    int nc = scan_float(val.c_str(), ret);
    if (nc != 1)
      throw bad_lexical_cast();
    return ret;
  }
  
  template <>
  std::string lexical_cast<std::string,long double>(const long double& val) {
    char* str = 0;
    int retval = print_float(str, val);
    if (retval == -1)
      throw bad_lexical_cast();
    std::string ret = str;
    free(str);
    return ret;
  }
  
  template <>
  long double lexical_cast<long double,std::string>(const std::string& val) {
    long double ret;
    int nc = scan_float(val.c_str(), ret);
    if (nc != 1)
      throw bad_lexical_cast();
    return ret;
  }
  
} // end of namespace boost
/*
// std::vector<> stream i/o
namespace std
{
  template <class Type>
  ostream& operator<<(ostream& out, const std::vector<Type>& vec) {
    for (size_t i = 0; i < vec.size(); ++i) {
      out << boost::lexical_cast<std::string>(vec[i]);
      if (i < vec.size() - 1)
        out << ", ";
    }
    return out;
  }
  
  template <class Type>
  istream& operator>>(istream& in, std::vector<Type>& vec) {
    //using namespace boost;
    //using namespace boost::algorithm;
    
    vec.clear();
    std::string data;
    getline(in, data);
    if (data == "")
      return in; // empty std::strings are OK
    std::vector<std::string> split_data;
    split(split_data, data, boost::is_any_of(","));
    for (size_t i = 0; i < split_data.size(); ++i) {
      boost::trim(split_data[i]);
      vec.push_back(boost::lexical_cast<Type>(split_data[i]));
    }
    return in;
  }
  
  // std::string vectors need special attention, since separators must be properly
  // escaped.
  template <>
  ostream& operator<<(ostream& out, const std::vector<std::string>& vec) {
    for (size_t i = 0; i < vec.size(); ++i) {
      std::string s = vec[i];
      // escape separators
      boost::replace_all(s, "\\", "\\\\");
      boost::replace_all(s, ", ", ",\\ ");
      
      out << s;
      if (i < vec.size() - 1)
        out << ", ";
    }
    return out;
  }
  
  template <>
  istream& operator>>(istream& in, std::vector<std::string>& vec) {
    //using namespace boost;
    //using namespace boost::algorithm;
    //using namespace boost::xpressive;
    
    vec.clear();
    std::string data;
    while (in.good()) {
      std::string line;
      getline(in, line);
      data += line;
    }
    
    if (data == "")
      return in; // empty string is OK
    
    boost::xpressive::sregex re = boost::xpressive::sregex::compile(", ");
    boost::xpressive::sregex_token_iterator iter(data.begin(), data.end(), re, -1), end;
    for (; iter != end; ++iter) {
      vec.push_back(*iter);
      // un-escape separators
      boost::replace_all(vec.back(), ",\\ ", ", ");
      boost::replace_all(vec.back(), "\\\\", "\\");
    }
    return in;
  }
}
*/

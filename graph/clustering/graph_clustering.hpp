//
//  graph_clustering.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/14.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef graph_clustering_h
#define graph_clustering_h

#include <boost/mpl/if.hpp>

#include "../config.h"
#include "../graph_filtering.hpp"
#include "../graph_selectors.hpp"
#include "../graph_properties.hpp"

#include "../demangle.hpp"
#include "../hash_map_wrap.hpp"
#include "../graph_adaptor.hpp"

#ifdef USING_OPENMP
#include <omp.h>
#endif

#ifndef __clang__
#include <ext/numeric>
using __gnu_cxx::power;
#else
template <class Value>
Value power(Value value, int n)
{
  return pow(value, n);
}
#endif

namespace bilab
{
  // calculates the number of triangles to which v belongs
  template <class Graph, class VProp>
  std::pair<int,int>
  get_triangles(typename boost::graph_traits<Graph>::vertex_descriptor v, VProp& mark,
                const Graph& g)
  {
    size_t triangles = 0;
    
    for (auto n : adjacent_vertices_range(v, g))
    {
      if (n == v)
        continue;
      mark[n] = true;
    }
    
    for (auto n : adjacent_vertices_range(v, g))
    {
      if (n == v)
        continue;
      for (auto n2 : adjacent_vertices_range(n, g))
      {
        if (n2 == n)
          continue;
        if (mark[n2])
          ++triangles;
      }
    }
    
    for (auto n : adjacent_vertices_range(v, g))
      mark[n] = false;
    
    size_t k = out_degree(v, g);
    if (is_directed::apply<Graph>::type::value)
      return std::make_pair(triangles, (k * (k - 1)));
    else
      return std::make_pair(triangles / 2, (k * (k - 1)) / 2);
  }
  
  
  // retrieves the global clustering coefficient
  struct get_global_clustering
  {
    template <class Graph>
    void operator()(const Graph& g, double& c, double& c_err) const
    {
      size_t triangles = 0, n = 0;
      std::vector<bool> mask(num_vertices(g), false);
      
#pragma omp parallel if (num_vertices(g) > OPENMP_MIN_THRESH) \
firstprivate(mask) reduction(+:triangles, n)
      parallel_vertex_loop_no_spawn
      (g,
       [&](auto v)
       {
         auto temp = get_triangles(v, mask, g);
         triangles += temp.first;
         n += temp.second;
       });
      c = double(triangles) / n;
      
      // "jackknife" variance
      c_err = 0.0;
      double cerr = 0.0;
#pragma omp parallel if (num_vertices(g) > OPENMP_MIN_THRESH) \
firstprivate(mask) reduction(+:cerr)
      parallel_vertex_loop_no_spawn
      (g,
       [&](auto v)
       {
         auto temp = get_triangles(v, mask, g);
         double cl = double(triangles - temp.first) /
         (n - temp.second);
         cerr += power(c - cl, 2);
       });
      c_err = sqrt(cerr);
    }
  };
  
  // sets the local clustering coefficient to a property
  struct set_clustering_to_property
  {
    template <class Graph, class ClustMap>
    void operator()(const Graph& g, ClustMap clust_map) const
    {
      typedef typename boost::property_traits<ClustMap>::value_type c_type;
      std::vector<bool> mask(num_vertices(g), false);
      
#pragma omp parallel if (num_vertices(g) > OPENMP_MIN_THRESH) \
firstprivate(mask)
      parallel_vertex_loop_no_spawn
      (g,
       [&](auto v)
       {
         auto triangles = get_triangles(v, mask, g);
         double clustering = (triangles.second > 0) ?
            double(triangles.first)/triangles.second : 0.0;
         //std::cout<<"c_type: "<< ::name_demangle(typeid(clust_map).name()) << std::endl;
         clust_map[v] = c_type(clustering);
       });
    }
    
    template <class Graph>
    struct get_undirected_graph
    {
      typedef typename boost::mpl::if_
      <
        std::is_convertible<typename boost::graph_traits<Graph>::directed_category,
        boost::directed_tag
      >,
      const boost::undirected_adaptor<Graph>,
      const Graph& >::type type;
    };
  };

  void global_clustering(bilab::IGraph& g, double& c, double& c_err)
  {
    run_action<bilab::detail::never_directed>()
    (g,
     std::bind(get_global_clustering(),
                  std::placeholders::_1,
                  std::ref(c),
                  std::ref(c_err)
                  )
     )();
  }
  
  //void local_clustering(bilab::IGraph& g,
  template<class ValueType, class IndexMap = bilab::IGraph::vertex_index_map_t>
  typename bilab::property_map_type::apply<ValueType, IndexMap>::type
  local_clustering(bilab::IGraph& g, bool undirected = true)
  {
    //bool found = false;
    //bilab::IGraph::vertex_index_map_t index_map;
    //size_t size = g.get_num_vertices();
    //size_t type_at_i = 0;
    //typedef typename bilab::property_map_type::apply<ValueType, IndexMap>::type Property;
    //Property prop;
    //typename bilab::property_map_type::apply<ValueType, IndexMap>::type prop;
    //boost::any prop;
    //boost::mpl::for_each<bilab::value_types>
    //(
    //  std::bind(
    //      new_property_map(),
    //      std::placeholders::_1,
    //      index_map,
    //      std::ref(type),
    //      std::ref(prop),
    //      size,
    //      std::ref(type_at_i),
    //      std::ref(found)
    //  )
    //);
    //if (!found) {
    //  throw ValueException("Invalid property type: " + type);
    //}
    // -----------------------------------------------------
    typedef typename bilab::property_map_type::apply<
      ValueType,
      IndexMap>::type Property;
    Property property;
    property.resize(g.get_num_vertices());
    //typedef bilab::vprop_map_t<double>::type Property;
    //Property property;
    //property.resize(g.get_num_vertices());
    //Property_g pe = g.get_vertex_index();
    //boost::any prop = boost::any_cast<Property>(g.get_vertex_index());
    boost::any prop = boost::any_cast<Property>(property);

    run_action<>()
    (g,
     std::bind(set_clustering_to_property(),
               std::placeholders::_1,
               std::placeholders::_2),
     writable_vertex_scalar_properties()
     )(prop);
    return boost::any_cast<Property>(prop);
    //g.set_vertex_filter_property(prop, false);
  }
} // namespace bilab


#endif /* graph_clustering_h */

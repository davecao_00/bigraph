//
//  config.h
//  biGraph
//
//  Created by 曹巍 on 2017/03/03.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef config_h
#define config_h

/* default minimum number of vertices for parallel loops */
#define OPENMP_MIN_THRESH 300
//#define USING_OPENMP 1

/*
#if defined(__GNUC__) && (__GNUC__ > 2) && defined(__OPTIMIZE__)
#define likely(expr) (__builtin_expect (!!(expr), 1))
#define unlikely(expr) (__builtin_expect (!!(expr), 0))
#define noinline __attribute__((noinline))
#define force_inline inline __attribute__((always_inline))
#define fastcall __attribute__((regparm(3)))
#define must_check __attribute__((warn_unused_result))
#define constant __attribute__((const))
#define pure __attribute__((pure))
#define tightly_packed __attribute__((__packed__))
#define flatten __attribute__((flatten))
#define nonnull __attribute__((nonnull))
#define page_aligned __attribute__((aligned(4096)))
#elif defined(__CLANG__)

#elif defined(__ICC)
#define noinline __attribute__((noinline))
#define force_inline inline __attribute__((always_inline))

#else
#define likely(expr) (expr)
#define unlikely(expr) (expr)
#define noinline
#define force_inline inline
#define fastcall
#define must_check
#define constant
#define pure
#define tighly_packed
#define flatten
#define nonnull
#define page_aligned
#endif

#if defined(__GNUC__) && ! defined(__ICC) && \
    (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 1))
#define BIGRAPH_ATTR_FLATTEN __attribute__((flatten))
#else
#define BIGRAPH_ATTR_FLATTEN
#endif

#if defined(__ICC)
#define 
#endif
*/

#endif /* config_h */

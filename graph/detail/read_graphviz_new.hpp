//
//  read_graphviz_new.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/07.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef read_graphviz_new_h
#define read_graphviz_new_h

#include <boost/ref.hpp>
#if (BOOST_VERSION >= 104000)
#   include <boost/property_map/dynamic_property_map.hpp>
#else
#   include <boost/dynamic_property_map.hpp>
#endif
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/detail/workaround.hpp>
#include <algorithm>
#include <string>
#include <vector>
#include <set>
#include <utility>
#include <map>
#include <iostream>
#include <cstdlib>

#include "graphviz_parser.hpp"

namespace boost
{
  namespace detail
  {
    namespace graph
    {
      // borrowed from graphml.hpp/graphviz.hpp
      class mutate_graph_bilab
      {
      public:
        virtual ~mutate_graph_bilab() {}
        virtual bool is_directed() const = 0;
        virtual void do_add_vertex(const node_t& node) = 0;
        
        virtual void
        do_add_edge(const edge_t& edge, const node_t& source, const node_t& target)
        = 0;
        
        virtual void
        set_node_property(const id_t& key, const node_t& node, const id_t& value) = 0;
        
        virtual void
        set_edge_property(const id_t& key, const edge_t& edge, const id_t& value) = 0;
        
        virtual void  // RG: need new second parameter to support BGL subgraphs
        set_graph_property(const id_t& key, const id_t& value) = 0;
      };
      
      template<typename MutableGraph>
      class mutate_graph_impl_bilab : public mutate_graph_bilab
      {
        typedef typename graph_traits<MutableGraph>::vertex_descriptor bgl_vertex_t;
        typedef typename graph_traits<MutableGraph>::edge_descriptor   bgl_edge_t;
        
      public:
        mutate_graph_impl_bilab(MutableGraph& graph,
                                dynamic_properties& dp,
                                std::string node_id_prop,
                                const std::unordered_set<std::string>& ignore_vp,
                                const std::unordered_set<std::string>& ignore_ep,
                                const std::unordered_set<std::string>& ignore_gp)
        : graph_(graph), dp_(dp), node_id_prop_(node_id_prop),
          m_ignore_vp(ignore_vp),
          m_ignore_ep(ignore_ep),
          m_ignore_gp(ignore_gp)
        {}
        
        ~mutate_graph_impl_bilab()
        {}
        
        bool is_directed() const
        {
          return
            boost::is_convertible<
              typename boost::graph_traits<MutableGraph>::directed_category,
              boost::directed_tag>::value;
        }
        
        virtual void do_add_vertex(const node_t& node)
        {
          // Add the node to the graph.
          bgl_vertex_t v = add_vertex(graph_);
          
          // Set up a mapping from name to BGL vertex.
          bgl_nodes.insert(std::make_pair(node, v));
          
          // node_id_prop_ allows the caller to see the real id names for nodes.
          put(node_id_prop_, dp_, v, node);
        }
        
        void
        do_add_edge(const edge_t& edge, const node_t& source, const node_t& target)
        {
          std::pair<bgl_edge_t, bool> result =
            add_edge(bgl_nodes[source], bgl_nodes[target], graph_);
          
          if(!result.second) {
            // In the case of no parallel edges allowed
            boost::throw_exception(bad_parallel_edge(source, target));
          } else {
            bgl_edges.insert(std::make_pair(edge, result.first));
          }
        }
        
        void
        set_node_property(const id_t& key, const node_t& node, const id_t& value)
        {
          if (m_ignore_vp.find(key) != m_ignore_vp.end())
            return;
          put(key, dp_, bgl_nodes[node], value);
        }
        
        void
        set_edge_property(const id_t& key, const edge_t& edge, const id_t& value)
        {
          if (m_ignore_ep.find(key) != m_ignore_ep.end())
            return;
          put(key, dp_, bgl_edges[edge], value);
        }
        
        void
        set_graph_property(const id_t& key, const id_t& value)
        {
          if (m_ignore_gp.find(key) != m_ignore_gp.end())
            return;
          /* RG: pointer to graph prevents copying */
          put(key, dp_, &graph_, value);
        }
        
        std::map<node_t, bgl_vertex_t> get_node_names() {
          return this->bgl_nodes;
        }
        
      protected:
        MutableGraph& graph_;
        dynamic_properties& dp_;
        std::string node_id_prop_;
        std::map<node_t, bgl_vertex_t> bgl_nodes;
        std::map<edge_t, bgl_edge_t> bgl_edges;
        const std::unordered_set<std::string>& m_ignore_vp;
        const std::unordered_set<std::string>& m_ignore_ep;
        const std::unordered_set<std::string>& m_ignore_gp;
      };
      
      
    } // namespace graph
  } // namespace detail

  namespace read_graphviz_detail {
/*
    typedef std::string node_name;
    typedef std::string subgraph_name;
    
    typedef std::map<std::string, std::string> properties;
    
    struct node_and_port_bi {
      node_name name;
      std::string angle; // Or empty if no angle
      std::vector<std::string> location; // Up to two identifiers
    };
    
    struct edge_info_bi {
      node_and_port source;
      node_and_port target;
      properties props;
    };
    
    struct parser_result_bi {
      bool graph_is_directed;
      bool graph_is_strict;
      std::map<node_name, properties> nodes; // Global set
      std::vector<edge_info> edges;
      std::map<subgraph_name, properties> graph_props; // Root and subgraphs
    };
*/    
    // The actual parser, from libs/graph/src/read_graphviz_new.cpp
    void parse_graphviz_from_string(
                                    const std::string& str,
                                    parser_result& result,
                                    int want_directed)
    {
      parser p(str, result);
      p.parse_graph(want_directed);
    }
    
    // Translate from those results to a graph
    void translate_results_to_graph_bilab(const parser_result& r,
                                    boost::detail::graph::mutate_graph_bilab* mg)
    {
      typedef boost::detail::graph::edge_t edge;
      for (std::map<node_name, properties>::const_iterator i = r.nodes.begin(); i != r.nodes.end(); ++i) {
        //std::cerr << i->first << " " << props_to_string(i->second) << std::endl;
        mg->do_add_vertex(i->first);
        for (properties::const_iterator j = i->second.begin(); j != i->second.end(); ++j) {
          mg->set_node_property(j->first, i->first, j->second);
        }
      }
      
      for (std::vector<edge_info>::const_iterator i = r.edges.begin(); i != r.edges.end(); ++i) {
        const edge_info& ei = *i;
        //std::cerr << ei.source << " -> " << ei.target << " " << props_to_string(ei.props) << std::endl;
        edge e = edge::new_edge();
        mg->do_add_edge(e, ei.source.name, ei.target.name);
        for (properties::const_iterator j = ei.props.begin(); j != ei.props.end(); ++j) {
          mg->set_edge_property(j->first, e, j->second);
        }
      }
      
      std::map<subgraph_name, properties>::const_iterator root_graph_props_i = r.graph_props.find("___root___");
      assert (root_graph_props_i != r.graph_props.end()); // Should not happen
      const properties& root_graph_props = root_graph_props_i->second;
      //std::cerr << "ending graph " << props_to_string(root_graph_props) << std::endl;
      for (properties::const_iterator i = root_graph_props.begin(); i != root_graph_props.end(); ++i) {
        mg->set_graph_property(i->first, i->second);
      }
    }
    
  } // namespace read_graphviz_detail
  
  namespace detail
  {
    namespace graph
    {
      BOOST_GRAPH_DECL bool read_graphviz(
                                          const std::string& str,
                                          boost::detail::graph::mutate_graph_bilab* mg,
                                          bool ignore_directedness)
      {
        read_graphviz_detail::parser_result parsed_file;
        read_graphviz_detail::parse_graphviz_from_string(str, parsed_file,
                                                         ignore_directedness ? 2 : mg->is_directed());
        read_graphviz_detail::translate_results_to_graph_bilab(parsed_file, mg);
        
        return parsed_file.graph_is_directed;
      }
    } // namespace graph
  } // namespace detail
  
  template <typename MutableGraph>
  bool read_graphviz(const std::string& str,
                     MutableGraph& graph,
                     boost::dynamic_properties& dp,
                     std::map<std::string, typename graph_traits<MutableGraph>::vertex_descriptor>& vp,
                     std::string const& node_id = "node_id",
                     bool ignore_directedness = false,
                     const std::unordered_set<std::string>& ignore_vp = std::unordered_set<std::string>(),
                     const std::unordered_set<std::string>& ignore_ep = std::unordered_set<std::string>(),
                     const std::unordered_set<std::string>& ignore_gp = std::unordered_set<std::string>())
  {
    boost::detail::graph::mutate_graph_impl_bilab<MutableGraph> mg(graph, dp, node_id,
                                                                   ignore_vp, ignore_ep,
                                                                   ignore_gp);
    bool r = detail::graph::read_graphviz(str, &mg, ignore_directedness);
    vp = mg.get_node_names();
    return r;
  }

  template <typename InputIter, typename MutableGraph>
  bool read_graphviz(InputIter begin, InputIter end,
                     MutableGraph& graph, boost::dynamic_properties& dp,
                     std::string const& node_id = "node_id",
                     bool ignore_directedness = false,
                     const std::unordered_set<std::string>& ignore_vp = std::unordered_set<std::string>(),
                     const std::unordered_set<std::string>& ignore_ep = std::unordered_set<std::string>(),
                     const std::unordered_set<std::string>& ignore_gp = std::unordered_set<std::string>())
  {
    return read_graphviz(std::string(begin, end), graph, dp, node_id,
                         ignore_directedness, ignore_vp, ignore_ep, ignore_gp);
  }
}// namespace boost
#endif /* read_graphviz_new_h */

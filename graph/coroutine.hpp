//
//  coroutine.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/22.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef coroutine_h
#define coroutine_h

#include "config.h"
#include <boost/version/hpp>

#ifdef HAVE_BOOST_COROUTINE
#    if (BOOST_VERSION >= 106200)
#        include <atomic>
#        include <boost/coroutine2/all.hpp>
namespace bilab
{
  namespace coroutines = boost::coroutines2;
}
#    else
#        include <boost/coroutine/all.hpp>
namespace bilab
{
  namespace coroutines = boost::coroutines;
}
#    endif
#endif // HAVE_BOOST_COROUTINE

#endif /* coroutine_h */

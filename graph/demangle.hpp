//
//  demangle.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/02/21.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef demangle_h
#define demangle_h

#include <cxxabi.h>
#include <cstdlib>

std::string name_demangle(std::string name);

#endif /* demangle_h */

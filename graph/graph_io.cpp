//
//  graph_io.cpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/10.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#include <string>
#include <boost/mpl/for_each.hpp>
#include <boost/any.hpp>

#include "graph_io.hpp"
//#include "graph_properties.hpp"


/*
template<class IndexMap>
template <class ValueType>
void bilab::io::check_value_type<IndexMap>::operator()(ValueType) {
  try {
    typedef typename bilab::property_map_type::apply<ValueType, IndexMap>::type
    map_t;
    map_t vector_map(_index_map);
    vector_map[_key] = boost::any_cast<ValueType>(_value);
    _map = new boost::detail::dynamic_property_map_adaptor<map_t>
    (vector_map);
  } catch (boost::bad_any_cast) {
  }
}

template <class VertexIndexMap, class EdgeIndexMap>
DP_SMART_PTR<boost::dynamic_property_map>
 bilab::io::create_dynamic_map<
  VertexIndexMap, EdgeIndexMap
>::operator()(const std::string&,
              const boost::any& key,
              const boost::any& value)
{
  boost::dynamic_property_map* map;
  try {
    boost::mpl::for_each<bilab::value_types>
    (check_value_type<VertexIndexMap>(_vertex_map,
                                      boost::any_cast<vertex_t>(key),
                                      value, map));
  } catch (boost::bad_any_cast) {
    try {
      boost::mpl::for_each<bilab::value_types>
      (check_value_type<EdgeIndexMap>(_edge_map,
                                      boost::any_cast<edge_t>(key),
                                      value, map));
    } catch (boost::bad_any_cast) {
      bilab::ConstantPropertyMap<size_t,boost::graph_property_tag> graph_index(0);
      boost::mpl::for_each<bilab::value_types>
      (check_value_type<bilab::ConstantPropertyMap<size_t,
       boost::graph_property_tag> >
       (graph_index, boost::any_cast<boost::graph_property_tag>(key),
        value, map));
    }
  }
  return DP_SMART_PTR<boost::dynamic_property_map>(map);
}
*/
void bilab::io::build_stream(
                  boost::iostreams::filtering_stream<
                    boost::iostreams::input>& stream,
                  const std::string& file,
                  std::ifstream& file_stream)
{
  stream.reset();
  if (file == "-"){
    stream.push(std::cin);
  } else {
    file_stream.open(file.c_str(), std::ios_base::in | std::ios_base::binary);
    file_stream.exceptions(std::ios_base::badbit | std::ios_base::failbit);
    
    if (boost::ends_with(file,".gz")){
      stream.push(boost::iostreams::gzip_decompressor());
    }
    
    if (boost::ends_with(file,".bz2")){
      stream.push(boost::iostreams::bzip2_decompressor());
    }
    stream.push(file_stream);
  }
  stream.exceptions(std::ios_base::badbit);
}

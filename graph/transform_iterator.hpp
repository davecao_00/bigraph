//
//  transform_iterator.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/02/23.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef transform_iterator_h
#define transform_iterator_h

#include <iterator>
#include <boost/iterator/transform_iterator.hpp>

template <class Predicate, class Iterator>
class transform_random_access_iterator
  : public boost::transform_iterator<Predicate, Iterator>
{
public:
  typedef Iterator iter_t;
  typedef boost::transform_iterator<Predicate, Iterator> base_t;
  transform_random_access_iterator() {}
  transform_random_access_iterator(const base_t& iter) :base_t(iter) {}
  transform_random_access_iterator(const Iterator& iter, const Predicate& pred = Predicate())
  : base_t(iter, pred) {}
};

namespace std
{
  template <class Predicate, class Iterator>
  struct iterator_traits<transform_random_access_iterator<Predicate, Iterator> >
  {
    typedef transform_random_access_iterator<Predicate, Iterator> titer_t;
    typedef typename titer_t::base_t base_t;
    typedef typename iterator_traits<base_t>::difference_type difference_type;
    typedef typename iterator_traits<base_t>::value_type value_type;
    typedef typename iterator_traits<base_t>::value_type reference;
    typedef typename iterator_traits<base_t>::pointer pointer;
    typedef typename iterator_traits<typename titer_t::iter_t>::iterator_category iterator_category;
  };
}

#endif /* transform_iterator_h */

//
//  graph_exceptions.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/02/21.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef graph_exceptions_h
#define graph_exceptions_h


#include <string>

namespace bilab {
  class GraphException : public std::exception
  {
  public:
    GraphException(const std::string& error){
      _error = error;
    };
    virtual ~GraphException() throw() {};
    virtual const char* what() const throw(){
      return _error.c_str();
    };
  protected:
    std::string _error;
  };
  
  class IOException: public GraphException
  {
  public:
    IOException(const std::string& error):GraphException(error)
    {};
    
    virtual ~IOException() throw()
    {};
  };

  class ValueException : public GraphException
  {
  public:
    ValueException(const std::string& error):GraphException(error)
    {};
    virtual ~ValueException() throw ()
    {};
  };

} // end of namespace bilab


#endif /* graph_exceptions_h */

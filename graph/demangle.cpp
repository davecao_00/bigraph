//
//  demangle.cpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/14.
//  Copyright © 2017年 巍 曹. All rights reserved.
//
#include <string>
#include "demangle.hpp"


std::string name_demangle(std::string name)
{
  int status = 0;
  char *realname = abi::__cxa_demangle(name.c_str(), 0, 0, &status);
  if (status != 0)
    return name + " (cannot demangle symbol)";
  std::string ret(realname);
  std::free(realname);
  return ret;
}

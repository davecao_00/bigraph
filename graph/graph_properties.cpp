//
//  graph_properties.cpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/15.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#include "graph_properties.hpp"
#include "graph_selectors.hpp"
#include "graph_filtering.hpp"

struct do_mark_edges
{
  template <class Graph, class EdgePropertyMap>
  void operator()(Graph& g, EdgePropertyMap prop) const
  {
    bilab::parallel_edge_loop
    (g,
     [&](auto e)
     {
       prop[e] = true;
     });
  }
};

void mark_edges(bilab::IGraph& gi, boost::any prop)
{
  bilab::run_action<>()
  (gi, std::bind<void>(do_mark_edges(), std::placeholders::_1, std::placeholders::_2),
   bilab::writable_edge_scalar_properties()) (prop);
}

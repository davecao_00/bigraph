//
//  graph_io.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/06.
//  Copyright © 2017年 巍 曹. All rights reserved.
//


#include <iostream>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/property_map/dynamic_property_map.hpp>
#include <boost/graph/graphml.hpp>
#include <boost/xpressive/xpressive.hpp>


// use correct smart pointer type for dynamic properties
#if (BOOST_VERSION / 100 % 1000 >= 44)
#define DP_SMART_PTR boost::shared_ptr
#else
#define DP_SMART_PTR std::auto_ptr
#endif

//#include "graph.hpp"
//#include "graph_filtering.hpp"
#include "graph_properties.hpp"
//#include "graph_util.hpp"


namespace bilab {
  
namespace io{
  
// this functor will check whether a value is of a specific type, create a
// corresponding vector_property_map and add the value to it

template <class IndexMap>
struct check_value_type
{
  typedef typename IndexMap::key_type key_t;
  
  check_value_type(IndexMap index_map,
                   const key_t& key,
                   const boost::any& value,
                   boost::dynamic_property_map*& map)
    :_index_map(index_map), _key(key), _value(value), _map(map)
  {}
  
  template <class ValueType>
  void operator()(ValueType){
    try {
      typedef typename bilab::property_map_type::apply<ValueType, IndexMap>::type
      map_t;
      map_t vector_map(_index_map);
      vector_map[_key] = boost::any_cast<ValueType>(_value);
      _map = new boost::detail::dynamic_property_map_adaptor<map_t>(vector_map);
    } catch (boost::bad_any_cast) {
    }
  }
  
  IndexMap _index_map;
  const key_t& _key;
  const boost::any& _value;
  boost::dynamic_property_map*& _map;
};

// this functor will check wether a key is a vertex or edge descriptor, and
// generate the corresponding property map, depending on the value type

template <class VertexIndexMap, class EdgeIndexMap>
struct create_dynamic_map
{
  typedef typename VertexIndexMap::key_type vertex_t;
  typedef typename EdgeIndexMap::key_type edge_t;
  
  create_dynamic_map(VertexIndexMap vertex_map, EdgeIndexMap edge_map)
    :_vertex_map(vertex_map), _edge_map(edge_map)
  {}
  
  DP_SMART_PTR<boost::dynamic_property_map>
  operator()(const std::string&,
             const boost::any& key,
             const boost::any& value)
    {
      boost::dynamic_property_map* map;
      try {
        boost::mpl::for_each<bilab::value_types>
          (check_value_type<VertexIndexMap>(_vertex_map,
                                            boost::any_cast<vertex_t>(key),
                                            value,
                                            map));
      } catch (boost::bad_any_cast) {
        try {
          boost::mpl::for_each<bilab::value_types>
            (check_value_type<EdgeIndexMap>(_edge_map,
                                            boost::any_cast<edge_t>(key),
                                            value,
                                            map));
        } catch (boost::bad_any_cast) {
          bilab::ConstantPropertyMap<size_t,boost::graph_property_tag> graph_index(0);
          boost::mpl::for_each<bilab::value_types>
            (check_value_type<
              bilab::ConstantPropertyMap<size_t, boost::graph_property_tag> >(
                              graph_index,
                              boost::any_cast<boost::graph_property_tag>(key),
                              value,
                              map));
        }
      }
      return DP_SMART_PTR<boost::dynamic_property_map>(map);
  }
  
  VertexIndexMap _vertex_map;
  EdgeIndexMap _edge_map;
};

// -------------------------
// read graph data from file
// -------------------------

void build_stream(
                  boost::iostreams::filtering_stream<
                  boost::iostreams::input>& stream,
                  const std::string& file,
                  std::ifstream& file_stream);
} // End of namespace io
} // End of namespace bilab

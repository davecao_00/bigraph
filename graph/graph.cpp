//
//  graph.cpp
//  biGraph
//
//  Created by 曹巍 on 2017/02/21.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#include <iostream>
#include <string>

#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>


#include "graph.hpp"
#include "graph_filtering.hpp"
#include "graph_properties.hpp"
#include "graph_selectors.hpp"

#include "graph_io.hpp"
#include "graphviz.hpp"

//using namespace std;
//using namespace boost;
//using namespace bilab;

// std::vector<> stream i/o
namespace std
{
  template <class Type>
  ostream& operator<<(ostream& out, const std::vector<Type>& vec) {
    for (size_t i = 0; i < vec.size(); ++i) {
      out << boost::lexical_cast<std::string>(vec[i]);
      if (i < vec.size() - 1)
        out << ", ";
    }
    return out;
  }
  
  template <class Type>
  istream& operator>>(istream& in, std::vector<Type>& vec) {
    //using namespace boost;
    //using namespace boost::algorithm;
    
    vec.clear();
    std::string data;
    getline(in, data);
    if (data == "")
      return in; // empty std::strings are OK
    std::vector<std::string> split_data;
    split(split_data, data, boost::is_any_of(","));
    for (size_t i = 0; i < split_data.size(); ++i) {
      boost::trim(split_data[i]);
      vec.push_back(boost::lexical_cast<Type>(split_data[i]));
    }
    return in;
  }
  
  // std::string vectors need special attention, since separators must be properly
  // escaped.
  template <>
  ostream& operator<<(ostream& out, const std::vector<std::string>& vec) {
    for (size_t i = 0; i < vec.size(); ++i) {
      std::string s = vec[i];
      // escape separators
      boost::replace_all(s, "\\", "\\\\");
      boost::replace_all(s, ", ", ",\\ ");
      
      out << s;
      if (i < vec.size() - 1)
        out << ", ";
    }
    return out;
  }
  
  template <>
  istream& operator>>(istream& in, std::vector<std::string>& vec) {
    //using namespace boost;
    //using namespace boost::algorithm;
    //using namespace boost::xpressive;
    
    vec.clear();
    std::string data;
    while (in.good()) {
      std::string line;
      getline(in, line);
      data += line;
    }
    
    if (data == "")
      return in; // empty string is OK
    
    boost::xpressive::sregex re = boost::xpressive::sregex::compile(", ");
    boost::xpressive::sregex_token_iterator iter(data.begin(), data.end(), re, -1), end;
    for (; iter != end; ++iter) {
      vec.push_back(*iter);
      // un-escape separators
      boost::replace_all(vec.back(), ",\\ ", ", ");
      boost::replace_all(vec.back(), "\\\\", "\\");
    }
    return in;
  }
}

struct reindex_vertex_property
{
  template <class PropertyMap, class IndexMap>
  void operator()(PropertyMap, const bilab::IGraph::multigraph_t& g,
                  boost::any map, IndexMap old_index, bool& found) const
  {
    try
    {
      PropertyMap pmap = boost::any_cast<PropertyMap>(map);
      for (size_t i = 0; i < num_vertices(g); ++i)
      {
        bilab::IGraph::vertex_t v = vertex(i, g);
        if (old_index[v] != int(i))
          pmap[v] = pmap[vertex(old_index[v], g)];
      }
      found = true;
    }
    catch (boost::bad_any_cast&) {}
  }
};

bilab::IGraph::IGraph()
  : _mg(std::make_shared<multigraph_t>()),
    _vertex_index(get(boost::vertex_index, *_mg)),
    _edge_index(get(boost::edge_index_t(), *_mg)),
    _reversed(false),
    _directed(true),
    _graph_index(0),
    _vertex_filter_map(_vertex_index),
    _vertex_filter_invert(false),
    _vertex_filter_active(false),
    _edge_filter_map(_edge_index),
    _edge_filter_invert(false),
    _edge_filter_active(false)
{}


// the destructor
bilab::IGraph::~IGraph()
{}

// this will get the number of vertices, either the "soft" O(1) way, or the hard
// O(V) way, which is necessary if the graph is filtered
size_t bilab::IGraph::get_num_vertices(bool filtered) {
  size_t n = 0;
  if (filtered && is_vertex_filter_active()){
    bilab::run_action<>()(*this, boost::lambda::var(n) =
                   boost::lambda::bind<size_t>(bilab::HardNumVertices(),boost::lambda::_1))();
  } else {
    n = num_vertices(*_mg);
  }
  return n;
}

// this will get the number of edges, either the "soft" O(E) way, or the hard
// O(E) way, which is necessary if the graph is filtered. Both cases are of
// linear complexity, since num_edges() is O(E) in Boost's adjacency_list
size_t bilab::IGraph::get_num_edges(bool filtered) {
  size_t n = 0;
  if (filtered && (is_edge_filter_active() || is_vertex_filter_active()))
    bilab::run_action<>()(*this, boost::lambda::var(n) =
                   boost::lambda::bind<size_t>(bilab::HardNumEdges(),boost::lambda::_1))();
  else
    n = num_edges(*_mg);
  return n;
}

struct clear_vertices
{
  template <class Graph>
  void operator()(Graph& g) const
  {
    int N = static_cast<int>(num_vertices(g));
    for (int i = N - 1; i >= 0; --i)
    {
      auto v = vertex(i, g);
      if (!is_valid_vertex(v, g))
        continue;
      remove_vertex(v, g);
    }
  }
};

void bilab::IGraph::clear()
{
  bilab::run_action<>()(*this, std::bind(clear_vertices(), std::placeholders::_1))();
}

struct do_clear_edges
{
  template <class Graph>
  void operator()(Graph& g) const
  {
    for (auto v : bilab::vertices_range(g))
      clear_vertex(v, g);
  }
};

void bilab::IGraph::clear_edges()
{
  bilab::run_action<>()(*this, std::bind(do_clear_edges(), std::placeholders::_1))();
}

void bilab::IGraph::re_index_vertex_property(boost::any map,
                                            boost::any aold_index) const
{
  typedef vprop_map_t<int64_t>::type index_prop_t;
  index_prop_t old_index = boost::any_cast<index_prop_t>(aold_index);
  
  bool found = false;
  boost::mpl::for_each<writable_vertex_properties>
  (std::bind(reindex_vertex_property(), std::placeholders::_1, std::ref(*_mg),
             map, old_index, std::ref(found)));
  if (!found)
    throw GraphException("invalid writable property map");
  
}

void bilab::IGraph::set_vertex_filter_property(boost::any property, bool invert) {
  try {
    _vertex_filter_map =
    boost::any_cast<vertex_filter_t::checked_t>(property).get_unchecked();
    _vertex_filter_invert = invert;
    _vertex_filter_active = true;
  } catch(boost::bad_any_cast&) {
    if (!property.empty())
      throw GraphException("Invalid vertex filter property!");
    _vertex_filter_active = false;
  }
}

void bilab::IGraph::set_edge_filter_property(boost::any property, bool invert) {
  try {
    _edge_filter_map =
    boost::any_cast<edge_filter_t::checked_t>(property).get_unchecked();
    _edge_filter_invert = invert;
    _edge_filter_active = true;
  } catch(boost::bad_any_cast&) {
    if (!property.empty())
      throw GraphException("Invalid edge filter property!");
    _edge_filter_active = false;
  }
}


// gets the correct graph view at run time
boost::any bilab::IGraph::get_graph_view() const {
  boost::any graph =
  check_filtered(*_mg,
                 _edge_filter_map,
                 _edge_filter_invert,
                 _edge_filter_active,
                 _mg->get_edge_index_range(),
                 _vertex_filter_map,
                 _vertex_filter_invert,
                 _vertex_filter_active,
                 const_cast<bilab::IGraph&>(*this),
                 _reversed,
                 _directed);
  return graph;
}

// these test whether or not the vertex and edge filters are active
bool bilab::IGraph::is_vertex_filter_active() const {
  return _vertex_filter_active;
}

bool bilab::IGraph::is_edge_filter_active() const {
  return _edge_filter_active;
}


// this function will reindex all the edges, in the order in which they are
// found
void bilab::IGraph::re_index_edges() {
  _mg->reindex_edges();
}

// this will definitively remove all the edges from the graph, which are being
// currently filtered out. This will also disable the edge filter
void bilab::IGraph::purge_edges() {
  if (!is_edge_filter_active())
    return;
  
  detail::MaskFilter<edge_filter_t> filter(_edge_filter_map,
                                           _edge_filter_invert);
  std::vector<
  boost::graph_traits<multigraph_t>::edge_descriptor
  > deleted_edges;
  
  for (auto v : bilab::vertices_range(*_mg)) {
    for (auto e : out_edges_range(v, *_mg))
      if (!filter(e))
        deleted_edges.push_back(e);
    for (auto& e  : deleted_edges)
      remove_edge(e, *_mg);
    deleted_edges.clear();
  }
}


// this will definitively remove all the vertices from the graph, which are
// being currently filtered out. This will also disable the vertex filter
void bilab::IGraph::purge_vertices(boost::any aold_index) {
  if (!is_vertex_filter_active())
    return;
  
  typedef vprop_map_t<int32_t>::type index_prop_t;
  index_prop_t old_index = boost::any_cast<index_prop_t>(aold_index);
  
  detail::MaskFilter<vertex_filter_t> filter(_vertex_filter_map,
                                             _vertex_filter_invert);
  size_t N = num_vertices(*_mg);
  std::vector<bool> deleted(N, false);
  for (size_t i = 0; i < N; ++i)
    deleted[i] = !filter(vertex(i, *_mg));
  std::vector<int> old_indexes;
  
  std::vector<boost::graph_traits<multigraph_t>::edge_descriptor> edges;
  
  //remove vertices
  for (int i = static_cast<int>(N-1); i >= 0; --i) {
    if (deleted[i]) {
      boost::graph_traits<multigraph_t>::vertex_descriptor v =
      vertex(i, *_mg);
      remove_vertex(v, *_mg);
    }
    else
    {
      old_indexes.push_back(i);
    }
  }
  
  N = old_indexes.size();
  for (int64_t i = N-1; i >= 0; --i)
    old_index[vertex((N - 1) - i, *_mg)] = old_indexes[i];
}

std::string bilab::IGraph::get_node_name(vertex_t v) {
  const std::map<std::string,vertex_t>::const_iterator it =
    std::find_if(this->vprops.begin(),
                 this->vprops.end(),
                 boost::bind(
                      &std::map<std::string,vertex_t>::value_type::second,
                             _1) == v);
  return it->first;
}
//File I/O

bool bilab::IGraph::read_from_file(std::string& file, const std::string& format,
                                   std::unordered_set<std::string> ivp,
                                   std::unordered_set<std::string> iep,
                                   std::unordered_set<std::string> igp) {
  
  if (format != "dot") {
    throw bilab::ValueException("error reading from file '" + file +
                                "': requested invalid format '" + format + "'");
  }
  try {
    boost::iostreams::filtering_stream<boost::iostreams::input> stream;
    std::ifstream file_stream;
    bilab::io::build_stream(stream, file, file_stream);
    
    bilab::io::create_dynamic_map<
      vertex_index_map_t,
      edge_index_map_t
    >
    map_creator(_vertex_index, _edge_index);
    // vertex property
    
    boost::dynamic_properties dp(map_creator);
    *_mg = multigraph_t();

    if (format == "dot"){
      _directed = read_graphviz(stream,
                                *_mg,
                                dp,
                                this->vprops,
                                "vertex_name", true, ivp, iep, igp);
      this->dp = dp;
    }
    // resize _vertex_filter_map, _edge_filter_map
    this->_vertex_filter_map.resize(this->get_num_vertices());
    return true;
  } catch (std::ios_base::failure &e) {
    throw IOException("error reading from file '" + file + "':" + e.what());
  } catch (boost::parse_error &e) {
    throw IOException("error reading from file '" + file + "':" + e.what());
  }
  return false;
}


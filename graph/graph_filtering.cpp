//
//  graph_filtering.cpp
//  biGraph
//
//  Created by 曹巍 on 2017/03/14.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#include "graph_filtering.hpp"
#include "demangle.hpp"

bool graph_filtering_enabled() {
  return true;
}

bilab::ActionNotFound::ActionNotFound(const std::type_info& action,
                                      const std::vector<const std::type_info*>& args)
  : bilab::GraphException(""), _action(action), _args(args)
{
  _error =
  "No static implementation was found for the desired routine. "
  "This is a . What follows is debug information.\n\n";
  
  _error += "Action: " + name_demangle(_action.name()) + "\n\n";
  for (size_t i = 0; i < _args.size(); ++i)
  {
    _error += "Arg " + boost::lexical_cast<std::string>(i+1) + ": " +
    name_demangle(_args[i]->name()) + "\n\n";
  }
}

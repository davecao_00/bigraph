//
//  graph_util.hpp
//  biGraph
//
//  Created by 曹巍 on 2017/02/22.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#ifndef graph_util_h
#define graph_util_h

#include <boost/graph/graph_traits.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/type_traits/is_convertible.hpp>
#include <boost/xpressive/xpressive.hpp>

#include <boost/functional/hash.hpp>

#include <functional>
#include <random>
#include <string>

#include "config.h"
//#include "graph_selectors.hpp"
//#include "graph_reverse.hpp"
#include "graph_filtered.hpp"


namespace bilab {
  // Forward declaration
  template <class Graph>
  struct out_edge_iteratorS;
  
  template <class Graph>
  inline __attribute__((always_inline)) __attribute__((flatten))
  auto out_edges_range(typename out_edge_iteratorS<Graph>::vertex_descriptor v,
                       const Graph& g);
  
  //
  // Metaprogramming
  // ===============
  
  // useful metafunction to determine whether a graph is directed or not
  
  struct is_directed
  {
    template <class Graph>
    struct apply
    {
      typedef std::is_convertible<typename boost::graph_traits<Graph>::directed_category,
      boost::directed_tag> type;
    };
  };
  
  
  // This will count "by hand" the number of vertices on a graph. Always O(V).
  struct HardNumVertices
  {
    template <class Graph>
    size_t operator()(Graph& g) const
    {
      size_t n = 0;
      typename boost::graph_traits<Graph>::vertex_iterator v_iter, v_begin, v_end;
      std::tie(v_begin, v_end) = vertices(g);
      for (v_iter = v_begin; v_iter != v_end; ++v_iter)
        n++;
      return n;
    }
  };
  
  // This will return the number of vertices on a graph, as given by
  // num_vertices. Can be O(1).
  struct SoftNumVertices
  {
    template <class Graph>
    size_t operator()(Graph& g) const { return num_vertices(g); }
  };
  
  // This will count "by hand" the number of edges on a graph. Always O(E).
  struct HardNumEdges
  {
    template <class Graph>
    size_t operator()(Graph& g) const
    {
      size_t n = 0;
      typename boost::graph_traits<Graph>::edge_iterator e_iter, e_begin, e_end;
      std::tie(e_begin, e_end) = edges(g);
      for (e_iter = e_begin; e_iter != e_end; ++e_iter)
        n++;
      return n;
    }
  };
  
  // This will return the number of edges on a graph, as given by num_edges. Can
  // be O(1).
  struct SoftNumEdges
  {
    template <class Graph>
    size_t operator()(Graph& g) const { return num_edges(g); }
  };
  
  // returns true if vertices u and v are adjacent. This is O(k(u)).
  template <class Graph>
  bool is_adjacent(typename boost::graph_traits<Graph>::vertex_descriptor u,
                   typename boost::graph_traits<Graph>::vertex_descriptor v,
                   const Graph& g )
  {
    for (const auto& e : out_edges_range(u, g))
    {
      if (target(e, g) == v)
        return true;
    }
    return false;
  }
  
  // computes the out-degree of a graph, ignoring self-edges
  template <class Graph>
  inline size_t
  out_degree_no_loops(typename boost::graph_traits<Graph>::vertex_descriptor v,
                      const Graph &g)
  {
    size_t k = 0;
    typename boost::graph_traits<Graph>::adjacency_iterator a,a_end;
    for (std::tie(a,a_end) = adjacent_vertices(v,g); a != a_end; ++a)
      if (*a != v)
        k++;
    return k;
  }
  
  // computes the out-degree of a graph, ignoring self-edges
  template <class Graph, class Weights>
  inline typename boost::property_traits<Weights>::value_type
  out_degree_no_loops_weighted(typename boost::graph_traits<Graph>::vertex_descriptor v,
                               Weights w, const Graph &g)
  {
    typename boost::property_traits<Weights>::value_type k = 0;
    typename boost::graph_traits<Graph>::out_edge_iterator e, e_end;
    for (std::tie(e, e_end) = out_edges(v, g); e != e_end; ++e)
      if (target(*e, g) != v)
        k += get(w, *e);
    return k;
  }
  
  
  template <class GraphOrig, class GraphTarget>
  void graph_copy(const GraphOrig& g, GraphTarget& gt)
  {
    typename boost::property_map<GraphOrig, boost::vertex_index_t>::type index = get(boost::vertex_index, g);
    typedef typename boost::graph_traits<GraphTarget>::vertex_descriptor tvertex_t;
    std::vector<tvertex_t> vmap(num_vertices(g));
    typename boost::graph_traits<GraphOrig>::vertex_iterator v, v_end;
    for (std::tie(v, v_end) = vertices(g); v != v_end; ++v)
      vmap[index[*v]] = add_vertex(gt);
    
    typename boost::graph_traits<GraphOrig>::edge_iterator e, e_end;
    for (std::tie(e, e_end) = edges(g); e != e_end; ++e)
      add_edge(vmap[index[source(*e, g)]], vmap[index[target(*e, g)]], gt);
  }
  
  
  //
  // Parallel loops
  // ==============
  
  template <class Graph, class F, size_t thres = OPENMP_MIN_THRESH>
  void parallel_vertex_loop_no_spawn(const Graph& g, F&& f)
  {
    size_t N = num_vertices(g);
#pragma omp for schedule(runtime)
    for (size_t i = 0; i < N; ++i)
    {
      auto v = vertex(i, g);
      if (!is_valid_vertex(v, g))
        continue;
      f(v);
    }
  }
  
  template <class Graph, class F, size_t thres = OPENMP_MIN_THRESH>
  void parallel_vertex_loop(const Graph& g, F&& f)
  {
#pragma omp parallel if (num_vertices(g) > thres)
    {
      parallel_vertex_loop_no_spawn<Graph, F, thres>(g, std::forward<F>(f));
    }
  }
  
  template <class Graph>
  const auto& get_dir(const Graph& g, std::true_type)
  { return g; }
  
  template <class Graph>
  const auto& get_dir(const Graph& g, std::false_type)
  { return g.original_graph(); }
  
  template <class Graph, class EPred, class VPred>
  auto get_dir(const boost::filt_graph<Graph, EPred, VPred>& g,
               std::false_type)
  {
    typedef typename
    std::remove_reference<decltype(g._g.original_graph())>::type g_t;
    return boost::filt_graph<g_t, EPred, VPred>(g._g.original_graph(),
                                                g._edge_pred,
                                                g._vertex_pred);
  }
  
  template <class Graph, class F, size_t thres = OPENMP_MIN_THRESH>
  void parallel_edge_loop_no_spawn(const Graph& g, F&& f)
  {
    auto&& u = get_dir(g, typename is_directed::apply<Graph>::type());
    typedef typename std::remove_const
    <typename std::remove_reference<decltype(u)>::type>::type graph_t;
    static_assert(is_directed::apply<graph_t>::type::value,
                  "graph must be directed at this point");
    auto dispatch =
    [&](auto v)
    {
      for (auto e : out_edges_range(v, u))
        f(e);
    };
    typedef decltype(dispatch) dispatch_t;
    parallel_vertex_loop_no_spawn<graph_t, dispatch_t&, thres>(u, dispatch);
  }
  
  template <class Graph, class F, size_t thres = OPENMP_MIN_THRESH>
  void parallel_edge_loop(const Graph& g, F&& f)
  {
#pragma omp parallel if (num_vertices(g) > thres)
    {
      parallel_edge_loop_no_spawn<Graph, F, thres>(g, std::forward<F>(f));
    }
  }
  
  template <class Container, class F, size_t thres = OPENMP_MIN_THRESH>
  void parallel_loop_no_spawn(Container&& v, F&& f)
  {
    size_t N = v.size();
#pragma omp for schedule(runtime)
    for (size_t i = 0; i < N; ++i)
      f(i, v[i]);
  }
  
  template <class Container, class F, size_t thres = OPENMP_MIN_THRESH>
  void parallel_loop(Container&& v, F&& f)
  {
#pragma omp parallel if (v.size() > thres)
    {
      parallel_loop_no_spawn<Container, F, thres>(std::forward<Container>(v),
                                                  std::forward<F>(f));
    }
  }
}

namespace std
{
  // STL omission?
  inline bool max(const bool& a, const bool& b) { return a || b; }
}

//
// Data type string representation
// ===============================
//
// String representation of individual data types. We have to take care
// specifically that no information is lost with floating point I/O.
//

namespace boost
{
  //
  // "chars" should be printed as numbers, since they can be non-printable
  //
  template <>
  std::string lexical_cast<std::string,uint8_t>(const uint8_t& val);
  template <>
  uint8_t lexical_cast<uint8_t,std::string>(const std::string& val);
  
  // float, double and long double should be printed in hexadecimal format to
  // preserve internal representation. (we also need to make sure the
  // representation is locale-independent).
  
  int print_float_dispatch(char*& str, float val);
  
  int print_float_dispatch(char*& str, double val);
  
  int print_float_dispatch(char*& str, long double val);
  
  template <class Val>
  int print_float(char*& str, Val val);
  
  int scan_float_dispatch(const char* str, float& val);
  
  int scan_float_dispatch(const char* str, double& val);
  
  int scan_float_dispatch(const char* str, long double& val);
  
  template <class Val>
  int scan_float(const char* str, Val& val);
  
  template <>
  std::string lexical_cast<std::string,float>(const float& val);
  
  template <>
  float lexical_cast<float,std::string>(const std::string& val);

  template <>
  std::string lexical_cast<std::string,double>(const double& val);

  template <>
  double lexical_cast<double,std::string>(const std::string& val);
  
  template <>
  std::string lexical_cast<std::string,long double>(const long double& val);
  
  template <>
  long double lexical_cast<long double,std::string>(const std::string& val);

} // end of namespace boost


// This will iterate over a random permutation of a random access sequence, by
// swapping the values of the sequence as it iterates
template <
  class RandomAccessIterator,
  class RNG,
  class RandomDist = std::uniform_int_distribution<size_t>
>
class random_permutation_iterator
: public std::iterator<
      std::input_iterator_tag,
      typename RandomAccessIterator::value_type>
{
public:
  random_permutation_iterator(RandomAccessIterator begin,
                              RandomAccessIterator end, RNG& rng)
  : _i(begin), _end(end), _rng(&rng)
  {
    if(_i != _end)
    {
      RandomDist random(0,  _end - _i - 1);
      std::iter_swap(_i, _i + random(*_rng));
    }
  }
  
  typename RandomAccessIterator::value_type operator*()
  {
    return *_i;
  }
  
  random_permutation_iterator& operator++()
  {
    ++_i;
    if(_i != _end)
    {
      RandomDist random(0,  _end - _i - 1);
      std::iter_swap(_i, _i + random(*_rng));
    }
    return *this;
  }
  
  bool operator==(const random_permutation_iterator& ri)
  {
    return _i == ri._i;
  }
  
  bool operator!=(const random_permutation_iterator& ri)
  {
    return _i != ri._i;
  }
  
  size_t operator-(const random_permutation_iterator& ri)
  {
    return _i - ri._i;
  }
  
private:
  RandomAccessIterator _i, _end;
  RNG* _rng;
};

/*
 // std::vector<> stream i/o
 namespace std
 {
 template <class Type>
 ostream& operator<<(ostream& out, const std::vector<Type>& vec);
 template <class Type>
 istream& operator>>(istream& in, std::vector<Type>& vec);
 
 // std::string vectors need special attention, since separators must be properly
 // escaped.
 template <>
 ostream& operator<<(ostream& out, const std::vector<std::string>& vec);
 template <>
 istream& operator>>(istream& in, std::vector<std::string>& vec);
 } // std namespace
 */

#endif /* graph_util_h */

//
//  main.cpp
//  biGraph
//
//  Created by 曹巍 on 2017/02/21.
//  Copyright © 2017年 巍 曹. All rights reserved.
//

#include <iostream>
#include <iomanip>

#include "cli_opt.hpp"
#include "stream_io/print.hpp"
#include "graph/graph.hpp"
#include "graph/clustering/graph_clustering.hpp"

// Code generation bugs cause tests to fail with global optimization.
#if BOOST_WORKAROUND(BOOST_MSVC, < 1300)
#pragma optimize("g", off)
#endif


int main(int argc, const char * argv[]) {

  // Parse command line arguments
  CLIARG::ParseCmdLine(argc, argv);
  
  // Define an undirected graph
  bilab::IGraph mg;
 
  // read graph data
  std::string suffix = "dot";
  
  // for output
  std::ofstream ofile;
  Print out;

  // manually start timer
  boost::timer::cpu_timer timer;
  
  if(CLIARG::verbose){
    timer.start();
  }
  if(!CLIARG::o_filename.empty()){
    ofile.open(CLIARG::o_filename.c_str(), std::ofstream::out);
    out(ofile);
  }else {
    out(std::cout);
  }

  bool read_ok = mg.read_from_file(CLIARG::i_filename, suffix, {}, {}, {});
  
  if (read_ok) {
    std::cout<< "Finish to read the graph data" << std::endl;
    std::cout<< "# of vertices: "<< mg.get_num_vertices() << std::endl;
    std::cout<< "# of edges: "<< mg.get_num_edges()<<std::endl;
  }else{
    std::cout<< "There were something wrong when reading the graph data" << std::endl;
  }

  if (CLIARG::verbose) {
    timer.stop();
    std::chrono::duration<double> seconds = std::chrono::nanoseconds(timer.elapsed().user);
    std::cout << "Loading the graph was completed in "<< seconds.count() << " s"<<std::endl;
   }
  
  // loop over vertices
  //bilab::IGraph::vertex_index_map_t vdp = mg.get_vertex_index();
  //const char* dot = "o_graphviz_test.dot";
  //boost::write_graphviz<bilab::IGraph::multigraph_t>>(dot, g);
  // graph action
  
  // g is an adjacent list.
  
   bilab::IGraph::multigraph_t g = mg.get_graph();
   //bilab::parallel_vertex_loop_no_spawn
   //(
   //	g,
   //	[&](auto v){
   //	   out << mg.get_node_name(v)
   //		   << ":"
   //		   << degree(v, g)
   //		   << std::endl;
   //	}
   //);
  ////
  //// loop over edges
  //bilab::parallel_edge_loop_no_spawn
  //(
  // g,
  // [&](auto e){
  //   auto s = source(e, g);
  //   auto t = target(e, g);
  //   out << mg.get_node_name(s)
  //       << "-"
  //       << mg.get_node_name(t)
  //       << std::endl;
  // }
  //);
  //
  //Global clustering coefficient
  double c, c_err;
  bilab::global_clustering(mg, c, c_err);
  out<< "Global clustering coefficient: "<<c << " std_err: "<< c_err<<std::endl;
  
  //Local clustering
  auto clus_map = bilab::local_clustering<double>(mg);
  bilab::parallel_vertex_loop_no_spawn
  (
   g,
   [&](auto v){
	 typename boost::graph_traits<
		 bilab::IGraph::multigraph_t
	 >::adjacency_iterator vi, vi_end;
	 
     out << mg.get_node_name(v)
         << ":"
         << degree(v, g)
         << ":"
         << clus_map[v]
		 << ":";
	 std::tie(vi, vi_end) = all_neighbours(v, g);
		//adjacent_vertices(v, g);
	 for(; vi != vi_end; ++vi) {
    	if(vi + 1 == vi_end){
          out<<mg.get_node_name(*vi);
        }else{
          out<<mg.get_node_name(*vi) << ",";
        }
     }
     out << std::endl;
   }
  );
  
  return 0;
}
